

// socket.io client side connection
const socket = io.connect();

// obtaining DOM elements from the Chat Interface
const messageForm = $('#message-form');
const messageBox = $('#message');
const chat = $('#chat');
const $theme = $('#theme');

// obtaining DOM elements from the NicknameForm Interface
const $nickForm = $('#nickForm');
const $nickError = $('#nickError');
const $name = $('#name');
const $nickname = $('#nickname');
const $password = $('#password');
const $role = $('#role');

// obtaining the usernames container DOM
const $users = $('#usernames');

$nickForm.submit( (e) => {
  let newUser = {
    name: $name.val(),
    nickname: $nickname.val(),
    password: $password.val(),
    role: $role.val()
  }
  console.log(newUser)
  e.preventDefault();
  socket.emit('new user', newUser, data => {
    if(data) {
      $('#nickWrap').hide();
      $('#contentWrap').show();
      $('#message').focus();
      $('#usernameChat').html(`<h4><i class="fas fa-user"></i> ${data.nickname}</h4>`)
    } else {
      $nickError.html(`
        <div class="alert alert-danger">
          That username already Exists, Please try another one.
        </div>
      `);
    }
  });
  $name.val('');
  $nickname.val('');
  $password.val('');

  setTimeout(() => {
    chat.scrollTop(chat[0].scrollHeight);
  }, 0); 
});

// events
messageForm.submit( (e) => {
  e.preventDefault();
  if(messageBox.val() != '' && messageBox.val().replace(/\s/g, '').length){
    socket.emit('send message', messageBox.val(), data => {
      chat.append(`<p class="error">${data}</p>`)
    });
  }
  messageBox.val('');
});

socket.on('new message', (data) => {
  displayMsg(data);
});

socket.on('usernames', (data) => {
  let html = '';
  for(i = 0; i < data.length; i++) {
    html += `<p><i class="fas fa-user"></i> ${data[i]}</p>`; 
  }
  $users.html(html);
});

socket.on('whisper', (data) => {
  chat.append(`<p class="whisper"><b>${data.nick}</b>: ${data.msg}</p>`);
});

socket.on('load old msgs', (msgs) => {
  for(let i = msgs.length -1; i >=0 ; i--) {
    let date = `${new Date(msgs[i].created).getHours()}:${new Date(msgs[i].created).getMinutes()} - 
      ${new Date(msgs[i].created).getDay()}/${new Date(msgs[i].created).getMonth()}/${new Date(msgs[i].created).getFullYear()}`;
    displayMsg(msgs[i], date);
  }
});

const displayMsg = (data, date) => {
  chat.append(`
    <div class="bg-light mb-2 pl-2" style="border-radius: 8px; color: black;">
      <p class="msg"><b>${data.nick}</b></p>
        <p class="pl-4">${data.msg}</p>
        <span class="badge badge-light pl-1" style="font-size: 10px; color: gray">${date}</span>
    </div>`
  );
}