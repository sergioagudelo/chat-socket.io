const mongoose = require('mongoose');
const { Schema } = mongoose;

const ChatSchema = new Schema({
  name: String,
  nick: String,
  password: String,
  role: String,
  msg: String,
  created: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Chat', ChatSchema);
